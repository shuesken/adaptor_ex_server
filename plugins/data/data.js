/**
 * Organize access to adaptor:ex data collections
 * 
 * data is a core plugin and will be included with every game
 * 
 * @requires plugin
 * @requires file
 * 
 * @module data/data
 * @copyright Lasse Marburg 2022
 * @license MIT
 */

const plugin = require('../plugin.js')
var schema = require('./schema.json')
const file = require('../../file.js')

/** @typedef {import('../../types').AdaptorAction} Action */

/**
* install request API for all custom collections the game contains
*/
class Data extends plugin.Plugin {
  constructor() {
    super(schema)
    this.name = "data"
    this.core = true
    this.collection_apis = []
  }

  /**
  * create request API for each custom database
  * 
  * collect files in file directory and watch for changes.
  */
  setup(config, game) {
    return new Promise((resolve, reject) => {
      super.setup(config, game)
      .then(result => {
        this.addTemplate("addItem", (payload, action) => {
          const title = `data ${action.name}`
          let subtitle = `Add new ${payload.collection} Item`
          const body = []
    
          if(payload.reference) {
            subtitle += ` as ${payload.reference}`
          }
    
          if (payload.variables) {
            body.push({text: JSON.stringify(payload.variables)})
          }
          
          return {title, subtitle, body}
        })

        log.debug(this.name, "load assets filenames from " + this.game.files)

        this.collectFiles(this.game.files)

        file.watchDir(this.game.files, (e, file) => {
          this.collectFiles(this.game.files)
          log.debug(this.name, "change in file directory: " + e + " " + file)
          this.schemaUpdate(this.schema)
        })

        for(let coll of this.game.setup.collections) {
          this.schema.definitions.collection.enum.push(coll)
          this.schema.definitions.collection.options.enum_titles.push(coll)
          this.schema.definitions.data_collection.enum.push(coll)
        }

        this.game.status.on("collection_added", data => {
          let updt = false
          if(!this.schema.definitions.collection.enum.includes(data.collection)) {
            this.schema.definitions.collection.enum.push(data.collection)
            if(data.plugin) {
              this.schema.definitions.collection.options.enum_titles.push(data.plugin + " " + data.name)
            } else {
              this.schema.definitions.collection.options.enum_titles.push(data.name)
            }
            updt = true
          }
          if(data.type == "data" && !this.schema.definitions.data_collection.enum.includes(data.collection)) {
            this.schema.definitions.data_collection.enum.push(data.collection)
            updt = true
          }
          if(updt) {
            this.schemaUpdate(this.schema)
          }
        })
        
        return resolve(this.schema)
      })
      .catch(err => {
        return reject(err)
      })
    })
  }

  collectFiles(path) {
    this.schema.definitions.files.anyOf[1].enum = []

    let files = file.ls(path, ".")

    for(let f of files) {
      this.schema.definitions.files.anyOf[1].enum.push(f)
    }

    let dirs = file.ls(path, "directories")

    for(let dir of dirs) {
      let files = file.ls(path + "/" + dir, ".")
      for(let fi of files) {
        this.schema.definitions.files.anyOf[1].enum.push(dir + "/" + fi)
      }
    }

    return this.schema.definitions.files.anyOf[1].enum
  }

  /**
   * @type {Action}
   * 
   * Call database update on an item
   */
  async update(data, session) {
    await session.variables.update(data.item, data.update, {multiple: data.multiple || false})
  }

  /**
   * @type {Action}
   * 
   * Set value of variable
   */
  async set(data, session) {
    await session.variables.set(data.variable, data.value, {multiple: data.multiple || false})
  }

  /**
   * @type {Action}
   * 
   * Increase numeric value of variable
   */
  async increase(data, session) {
    await session.variables.edit("inc", data.variable, data.value, {multiple: data.multiple || false})
  }

  /**
   * @type {Action}
   * 
   * Add element to variable list
   */
  async push(data, session) {
    if(data.duplicates) {
      await session.variables.push(data.variable, data.value, {multiple: data.multiple || false})
    } else {
      await session.variables.add(data.variable, data.value, {multiple: data.multiple || false})
    }
  }

  /**
   * @type {Action}
   * 
   * Remove Element from variable list
   */
  async pull(data, session) {
    await session.variables.pull(data.variable, data.value, {multiple: data.multiple || false})
  }

  /**
   * @type {Action}
   * 
   * Create a new Item in one of the existing data collections
   */
  async addItem(data, session) {
    await session.variables.create(data.collection, data.variables, data.reference)
  }

  /**
   * @type {Action}
   * 
   * Remove variable from item document or delete the whole document.
   */
  async delete(data, session) {
    await session.variables.delete(data.variable, {multiple: data.multiple || false})
  }

  /**
   * @type {Action}
   * 
   * create new reference to current session in an external document.
   * 
   * document can than be accessed during session via name reference.
   */
   async getItem(data, session) {
    if(!adaptor.hasOwnProperties(data, ["collection", "query", "reference"])) {
      throw new adaptor.InvalidError("can not load reference. collection, query or reference property missing.")
    }

    data.query = await session.variables.review(data.query)

    await session.createReference(data["collection"], data.query, data["reference"], {multiple: data.multiple})

    session.log(`create ${data.collection} reference "${data.reference}"`)
  }
}

module.exports.Plugin = Data
