{
  "settings":{
    "type":"object",
    "title":"Device Settings",
    "properties":{
    }
  },
  "definitions":{
    "schema":{},
    "init":{
      "type":"object",
      "required":["level","reference"],
      "description":"Set a level that is started if a player is referenced in a device message. If message contains json with reference key, the level is started with the matching or a new player as first variable agument.",
      "properties":{
        "level":{
          "type":"string",
          "enum":[]
        },
        "reference":{
          "type":"string"
        }
      }
    }
  },
  "actions":{
    "send":{
      "type": "object",
      "title": "Send Message",
      "description": "Send a Message to your device.",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/devices/send.html",
      "required":["to"],
      "default":{"to":"","message":""},
      "additionalProperties":false,
      "properties": {
        "to":{
          "type":"string",
          "enum":[]
        },
        "message":{
          "type":["string","number","boolean","object","array"]
        },
        "is_float":{
          "type":"boolean",
          "format":"checkbox",
          "description":"specify if number message should be send as floating point value."
        },
        "path":{
          "type":["string"]
        },
        "method":{
          "type":"string",
          "description":"Method requests to your HTTP device will be send with.",
          "enum":["GET","POST"]
        }
      }
    }
  },
  "data_variables": {
    "http": {
      "incoming": {
        "type": ["string","number","integer","boolean","object","array"]
      }
    },
    "tcp": {
      "incoming": {
        "type": ["string","number","integer","boolean","object","array"]
      }
    },
    "udp": {
      "incoming": {
        "type": ["string","number","integer","boolean","object","array"]
      }
    },
    "osc": {
      "incoming": {
        "type": ["string","number","integer","boolean","object","array"]
      }
    },
    "serial": {
      "incoming": {
        "type": ["string","number","integer","boolean","object","array"]
      }
    }
  },
  "collections":{
    "http":{
      "type":"object",
      "title":"HTTP Device",
      "required":["settings","name"],
      "events":{
        "incomingMessage":{}
      },
      "properties":{
        "name": {
          "type":"string",
          "pattern":"^[0-9A-Za-z_-]*$",
          "propertyOrder":1
        },
        "settings":{
          "type":"object",
          "additionalProperties":false,
          "properties":{
            "to_device":{
              "type":"object",
              "required":["url"],
              "additionalProperties":false,
              "properties":{
                "url":{
                  "type":"string",
                  "pattern":"https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}(\\.[a-zA-Z0-9()])?\\b([-a-zA-Z0-9()!@:%_\\+.~#?&\\/\\/=]*)",
                  "description":"Set url adaptor will send requests to. E.g. http://yourdomain.com:8080/mydevice"
                },
                "method": {
                  "type":"string",
                  "description":"Method requests to your device will be send with.",
                  "enum":["GET","POST"]
                }
              }
            },
            "from_device":{
              "type":"object",
              "additionalProperties":false,
              "properties":{
                "method": {
                  "type":"string",
                  "description":"Method the device sends requests to adaptor.",
                  "enum":["GET","POST"]
                },
                "webhook":{
                  "title":"Webhook URLs",
                  "$ref":"#/definitions/webhooks"
                }
              }
            }
          }
        }
      }
    },
    "tcp":{
      "type":"object",
      "title":"TCP Device",
      "events":{
        "incomingMessage":{}
      },
      "required":["settings","name"],
      "properties":{
        "name": {
          "type":"string",
          "pattern":"^[0-9A-Za-z_-]*$",
          "propertyOrder":1
        },
        "settings":{
          "type":"object",
          "additionalProperties":false,
          "properties":{
            "port":{
              "type":"integer",
              "title":"Device Port",
              "default":0,
              "description":"TCP port the device is listening to."
            },
            "ip":{
              "type":"string",
              "title":"Device IP",
              "description":"Set the ip address of your device so adaptor:ex can send messages to it."
            },
            "adaptor_port":{
              "type":"integer",
              "title":"Adaptor Port",
              "description":"Set a port on which adaptor listens for messages from the TCP device."
            }
          }
        }
      }
    },
    "udp":{
      "type":"object",
      "title":"UDP Device",
      "events":{
        "incomingMessage":{}
      },
      "required":["settings","name"],
      "properties":{
        "name": {
          "type":"string",
          "pattern":"^[0-9A-Za-z_-]*$",
          "propertyOrder":1
        },
        "settings":{
          "type":"object",
          "additionalProperties":false,
          "properties":{
            "port":{
              "type":"integer",
              "title":"Device Port",
              "default":0,
              "description":"UDP port the device is listening to."
            },
            "ip":{
              "type":"string",
              "title":"Device IP",
              "description":"Set the ip address of your device so adaptor can send messages to it.",
              "default":"0.0.0.0"
            },
            "adaptor_port":{
              "type":"integer",
              "title":"Adaptor Port",
              "description":"Set a port on which adaptor listens for messages from the UDP device."
            }
          }
        }
      }
    },
    "osc":{
      "type":"object",
      "title":"OSC Device",
      "events":{
        "incomingMessage":{}
      },
      "required":["settings","name"],
      "properties":{
        "name": {
          "type":"string",
          "pattern":"^[0-9A-Za-z_-]*$",
          "propertyOrder":1
        },
        "settings":{
          "type":"object",
          "additionalProperties":false,
          "properties":{
            "port":{
              "type":"integer",
              "title":"Device Port",
              "default":0,
              "description":"OSC port the device is listening to."
            },
            "ip":{
              "type":"string",
              "title":"Device IP",
              "description":"Set the ip address of your device so adaptor can send messages to it.",
              "default":"0.0.0.0"
            },
            "path":{
              "type":"string",
              "title":"Prepend Path",
              "description":"Set an OSC formatted address that is prepended to every message sent to the OSC device"
            },
            "adaptor_port":{
              "type":"integer",
              "title":"Adaptor Port",
              "description":"Set a port on which adaptor listens for messages from the OSC device."
            }
          }
        }
      }
    },
    "serial":{
      "type":"object",
      "title":"Serial Device",
      "events":{
        "incomingMessage":{}
      },
      "required":["settings","name"],
      "properties":{
        "name": {
          "type":"string",
          "pattern":"^[0-9A-Za-z_-]*$",
          "propertyOrder":1
        },
        "settings":{
          "type":"object",
          "additionalProperties":false,
          "required":["port","baud"],
          "properties":{
            "port":{
              "type":"string",
              "title":"Serial Port",
              "description":"Serial port of connected device (e.g. '/dev/ttyACM0' or 'COM0')",
              "enum":["No port available"]
            },
            "baud":{
              "type":"number",
              "title":"Baud Rate",
              "default":115200,
              "enum":[300, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 74880, 115200, 230400, 250000, 500000, 1000000, 2000000]
            }
          }
        }
      }
    }
  }
}
