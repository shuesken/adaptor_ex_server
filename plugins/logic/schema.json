{
  "settings":{
    "type":"object",
    "title":"Logic",
    "description": "Switch states based on conditions. Install event listeners.",
    "additionalProperties": false,
    "readonly": true
  },
  "definitions": {
    "next":{
      "type":"string",
      "title":"Next State",
      "format":"adaptorState",
      "description":"Goto next state. Close all listeners in this node.",
      "isAdaptorContent":false
    },
    "function":{
      "type":"string",
      "isAdaptorContent":false,
      "enum":[]
    },
    "field":{
      "type":"string",
      "propertyOrder":1,
      "description":"Define a field inside message with json object. Use 'dot.notation' for nested fields."
    },
    "value":{
      "type":"string",
      "required":true,
      "propertyOrder": 1
    },
    "contains":{
      "propertyOrder": 2,
      "title":"contains any of",
      "type":"array",
      "minItems":1,
      "format":"table",
      "default":[""],
      "items":{"type":"string","title":"contains"}
    },
    "equals":{
      "propertyOrder": 2,
      "title":"equals any of",
      "type":"array",
      "minItems":1,
      "format":"table",
      "default":[""],
      "items":{"type":"string","title":"equals"}
    },
    "lessThan":{
      "propertyOrder": 2,
      "title":"is less than any of",
      "type":"array",
      "minItems":1,
      "format":"table",
      "default":[""],
      "items":{"type":"string","title":"less than","isAdaptorContent":false}
    },
    "greaterThan":{
      "propertyOrder": 2,
      "title":"is greater than any of",
      "type":"array",
      "minItems":1,
      "format":"table",
      "default":[""],
      "items":{"type":"string","title":"greater than","isAdaptorContent":false}
    },
    "regex":{
      "type":"string",
      "content":true,
      "required":true
    },
    "javascript":{
      "type":"string",
      "isAdaptorContent":false,
      "format":"textarea",
      "default":"if(value){\n  return true\n}"
    },
    "find":{
      "type":"object",
      "required":["collection","query"],
      "properties":{
        "collection":{"$ref":"#/definitions/collection"},
        "query":{
          "type":"string",
          "isAdaptorContent":false
        }
      }
    }
  },
  "actions": {
    "switch": {
      "type":"object",
      "title":"Switch",
      "description":"Check the state of a variable and switch to the next state if the checked condition is true.",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/logic/switch.html",
      "required":["if"],
      "resolveAdaptorVariables":false,
      "additionalProperties":false,
      "properties":{
        "if":{
          "title":"if",
          "type":"array",
          "items":{
            "title":"condition",
            "oneOf":[
              {
                "title":"Equals",
                "description":"Trigger next if value equals",
                "type":"object",
                "additionalProperties": false,
                "required":["value","equals","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "equals":{"$ref":"#/definitions/equals"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Contains",
                "description":"Trigger next if value contains",
                "type":"object",
                "additionalProperties": false,
                "required":["value","contains","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "contains":{"$ref":"#/definitions/contains"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Less Than",
                "description":"Trigger next if value is less",
                "type":"object",
                "additionalProperties": false,
                "required":["value","lessThan","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "lessThan":{"$ref":"#/definitions/lessThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Greater Than",
                "description":"Trigger next if value is greater",
                "type":"object",
                "additionalProperties": false,
                "required":["value","greaterThan","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "greaterThan":{"$ref":"#/definitions/greaterThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Regular Expression",
                "description":"If regular expression returns anything when used on value",
                "type":"object",
                "additionalProperties": false,
                "required":["value","regex","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "regex":{"$ref":"#/definitions/regex"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Javascript function",
                "description":"If javascript function returns true. Use value as function argument.",
                "type":"object",
                "additionalProperties": false,
                "required":["javascript","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "javascript":{"$ref":"#/definitions/javascript"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Database Query",
                "description":"If value equals value in path of first query document that is returned.",
                "type":"object",
                "additionalProperties": false,
                "required":["find","next"],
                "properties":{
                  "find":{"$ref":"#/definitions/find"},
                  "next":{"$ref":"#/definitions/next"}
                }
              }
            ]
          }
        },
        "else": {
          "required":["next"],
          "properties":{
            "next":{"$ref":"#/definitions/next"}
          }
        }
      }
    },
    "onChange": {
      "title":"On Change",
      "description":"Listen for changes in Data sets and variables.",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/logic/onChange.html",
      "mode":"listen",
      "type":"object",
      "resolveAdaptorVariables":false,
      "required":["if"],
      "properties":{
        "if":{
          "title":"if",
          "type":"array",
          "items":{
            "title":"condition",
            "oneOf":[
              {
                "title":"Equals",
                "description":"Trigger next if value equals",
                "type":"object",
                "additionalProperties": false,
                "required":["value","equals","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "equals":{"$ref":"#/definitions/equals"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Contains",
                "description":"Trigger next if value contains",
                "type":"object",
                "additionalProperties": false,
                "required":["value","contains","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "contains":{"$ref":"#/definitions/contains"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Less Than",
                "description":"Trigger next if value is less",
                "type":"object",
                "additionalProperties": false,
                "required":["value","lessThan","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "lessThan":{"$ref":"#/definitions/lessThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Greater Than",
                "description":"Trigger next if value is greater",
                "type":"object",
                "additionalProperties": false,
                "required":["value","greaterThan","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "greaterThan":{"$ref":"#/definitions/greaterThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Regular Expression",
                "description":"If regular expression returns anything when used on value",
                "type":"object",
                "additionalProperties": false,
                "required":["value","regex","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "regex":{"$ref":"#/definitions/regex"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Javascript function",
                "description":"If javascript function returns true. Use value as function argument.",
                "type":"object",
                "additionalProperties": false,
                "required":["javascript","next"],
                "properties":{
                  "value":{"$ref":"#/definitions/value"},
                  "javascript":{"$ref":"#/definitions/javascript"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Database Query",
                "description":"If value equals value in path of first query document that is returned.",
                "type":"object",
                "additionalProperties": false,
                "required":["find","next"],
                "properties":{
                  "find":{"$ref":"#/definitions/find"},
                  "next":{"$ref":"#/definitions/next"}
                }
              }
            ]
          }
        }
      }
    },
    "onEvent": {
      "type":"object",
      "title":"On Event",
      "description":"Listen for events from this level, items or remote sessions or global events.",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/logic/onEvent.html",
      "mode":"listen",
      "resolveAdaptorVariables":false,
      "id":"onEvent",
      "required":["event"],
      "default":{"event":"", "from":"session"},
      "additionalProperties": false,
      "properties":{
        "from":{"$ref":"#/definitions/event_source"},
        "event":{"$ref":"#/definitions/event_name"},
        "if":{
          "title":"if",
          "type":"array",
          "items":{
            "title":"condition",
            "oneOf":[
              {
                "title":"Equals",
                "description":"Trigger next if event message equals",
                "type":"object",
                "additionalProperties": false,
                "required":["equals","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "equals":{"$ref":"#/definitions/equals"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Contains",
                "description":"Trigger next if event message contains",
                "type":"object",
                "additionalProperties": false,
                "required":["contains","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "contains":{"$ref":"#/definitions/contains"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Less Than",
                "description":"Trigger next if event message is less than",
                "type":"object",
                "additionalProperties": false,
                "required":["lessThan","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "lessThan":{"$ref":"#/definitions/lessThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Greater Than",
                "description":"Trigger next if event message is greater than",
                "type":"object",
                "additionalProperties": false,
                "required":["greaterThan","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "greaterThan":{"$ref":"#/definitions/greaterThan"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Javascript function",
                "description":"If javascript function returns true. Use value as function argument.",
                "type":"object",
                "additionalProperties": false,
                "required":["javascript","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "javascript":{"$ref":"#/definitions/javascript"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Regular Expression",
                "description":"If regular expression matches anything when used on event message",
                "type":"object",
                "additionalProperties": false,
                "required":["regex","next"],
                "properties":{
                  "field":{"$ref":"#/definitions/field"},
                  "regex":{"$ref":"#/definitions/regex"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              }
            ]
          }
        },
        "else": {
          "type":"object",
          "properties":{
            "next":{"$ref":"#/definitions/next"}
          }
        },
        "keep_listening": {"$ref":"#/definitions/keep_listening"}
      }
    },
    "dispatchEvent": {
      "type": "object",
      "title":"Dispatch Event",
      "description":"Trigger an event that can be responded to elsewhere with On Event. Event will be dispatched in current session if no event source is defined.",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/logic/dispatch.html",
      "template":"{let body; if(payload.message) {body = [{ text: `Message: ${payload.message.toString()}` }]}; return { title:`logic ${action.name}`, subtitle:`Event '${payload.name}' from ${payload.source || 'session'}`, body:body} }",
      "additionalProperties":false,
      "required":["name"],
      "default":{"name":"", "source":"session"},
      "properties": {
        "name":{
          "type":"string",
          "description":"Name of new event to be dispatched"
        },
        "source":{
          "type":"string",
          "default":"game",
          "description":"Dispatch event from any item or session or dispatch a global game event with 'game'"
        },
        "message":{
          "type":["string","number","integer","array","object"],
          "description":"Add any type of data that is supposed to be send with the event. Valid json in string will be parsed to object."
        }
      }
    },
    "function": {
      "type":"object",
      "title":"Function",
      "description":"Call a custom or basic function",
      "documentation":"https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/basics/actions/logic/function.html",
      "default":{"function":"","arguments":""},
      "required":["function"],
      "additionalProperties":false,
      "properties":{
        "function":{"$ref":"#/definitions/function"},
        "arguments":{
          "type":"array",
          "format":"table",
          "description":"Add argument data that will be forwarded to the function.",
          "items":{
            "title":"arg",
            "default":""
          }
        },
        "next":{
          "type":"array",
          "format":"table",
          "description":"If function returns `next: <index>` the next at <index> (zero based) will be dispatched.",
          "items":{
            "$ref":"#/definitions/next"
          }
        }
      }
    }
  },
  "action_variables":{
    "switch":{
      "match":{"type":["string","number","integer"]}
    },
    "onChange":{
      "match":{"type":["string","number","integer"]}
    },
    "onEvent":{
      "name":{"type":"string"},
      "match":{"type":["string","number","integer"]},
      "message":{"type":["string","number","integer","object","array"]}
    },
    "function":{
    }
  }
}
