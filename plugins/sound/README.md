Sound
=====

Uses npm package [play-sound](https://www.npmjs.com/package/play-sound) to play audio files on the local machine.

For use on a Windows it is most likely required to install a mediaplayer with command line functionality. We recommend to use [mplayer](http://www.mplayerhq.hu/design7/dload.html#binaries).


Install mplayer for Windows
----------------------------

Download [mplayer for windows](http://mplayerwin.sourceforge.net/downloads.html) matching your system architecture (64 or 32bit).

Extract it to your Program files directory and copy the path to the directory.

Create a new System Environment path variable and paste the path to your mplayer directory. 

Restart powershell and adaptor:ex server.