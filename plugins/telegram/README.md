Telegram
========

Create a Client
---------------

Follow this guide on how to create a Telegram development API ID and hash: https://docs.telethon.dev/en/stable/basic/signing-in.html

Add ID and hash to the telegram plugin settings. Now you should be able to create new telegram clients.

Make sure to install each client on a classical Telegram Desktop or Mobile App first.

Once you have a client installed, create a telegram client in adaptor and fill out `phone_number` in settings. Save the client and click the connect button. Adaptor will ask you for a login authentication code. You should have received it in your Telegram App.

More detailed instructions can be found in the [adaptor:ex documentation](https://machina_ex.gitlab.io/adaptor_ex/adaptor_ex_tutorials/tutorials/telegram-settings/index.html)

Emojis
-------

Telegram uses Apple emojis. To use emoticons when sending messages you can

look them up on this page:

https://emojipedia.org/

or use this page:

https://www.emojicopy.com/

Search the wanted emoji and copy it. Use the `copy` button to copy it, then paste it into the message field.

Same works with the Telegram Desktop or Web Client. Simply write a message with the wanted emoji and copy the message.

You should see a unicode representation or some kind of icon in the textfield. The representation you see depends on the browser you use to run the adaptor editor.
