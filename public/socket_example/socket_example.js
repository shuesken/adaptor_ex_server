let base_url = window.location.origin

let game = "Tutorial"

let headers = new Headers({
  'Authorization': 'Basic ' + btoa("lasse:abcd1234")
})

//adaptor_socket = io(base_url, { query: `browser=${identifyBrowser()}`})

log_socket = io(`${base_url}/log`)

game_socket = io(`${base_url}/game/${game}`)

sessions_socket = io(`${base_url}/game/${game}/live/sessions`)

items_socket = io(`${base_url}/game/${game}/live/items`)

session_socket = io(`${base_url}/game/${game}/session/61430edb4a97332856ab94d6`)

level_socket = io(`${base_url}/game/${game}/level/FO9D62AfhwsUae8T`)

zentrale = io(`${base_url}/game/${game}/livingroom/zentrale`)

satellit = io(`${base_url}/game/${game}/livingroom/satellit`)

/*
adaptor_socket.on('connect', data => {
	console.log("adaptor socket connected")
})
*/

function storyInput(type, text, option_id) {
  zentrale.emit("storyInput", {type:type, text: text, option: option_id})
}

function toZentrale(topic, message) {
  zentrale.emit(topic, message)
}

function toSatellit(topic, message) {
  satellit.emit(topic, message)
}


log_socket.on('connect', data => {
	console.log("log socket connected")
  log_socket.emit("level", "info")
  log_socket.emit("history", 5)
})

game_socket.on('connect', data => {
	console.log("game socket connected")
})

sessions_socket.on('connect', data => {
	console.log("sessions socket connected")
})

items_socket.on('connect', data => {
	console.log("items socket connected")
})

session_socket.on('connect', data => {
	console.log("session socket connected")
})

level_socket.on('connect', data => {
	console.log("level socket connected")
})

zentrale.on('connect', data => {
	console.log("Zentrale socket connected")

  //zentrale.emit("joinTeam", {team:"rednecks"})
})

zentrale.on('message', msg => {
  console.log("Zentrale Message: " + msg)
})

zentrale.on('storyMessage', msg => {
  console.log("Zentrale Story Message: ")
  console.log(msg)
})

zentrale.onAny((topic, msg) => {
  console.log("Zentrale got topic '" + topic + "': ")
  console.log(msg)
})

zentrale.on('disconnect', data => {
	console.log("Zentrale socket disconnected")
})

satellit.on('connect', data => {
	console.log("Satellit socket connected")

  // satellit.emit("joinTeam", {team:"rednecks", player:"Lasse"})
})

satellit.on('disconnect', data => {
	console.log("Satellit socket disconnected")
})


satellit.on('playAudio', msg => {
  console.log("playAudio: " + msg)
})

satellit.on('loadAudio', msg => {
  console.log("loadAudio:")
  console.log(msg)
})

satellit.on('stopAudio', msg => {
  console.log("stopAudio: " + msg)
})

satellit.onAny((topic, msg) => {
  console.log("Satellit got topic '" + topic + "': ")
  console.log(msg)
})


/**
* show warning on disconnect
*/
/*
adaptor_socket.on('disconnect', data => {
	console.error('adaptor socket disconnected')
})
*/

log_socket.on('log', data => {
	console.log(data)
})

game_socket.onAny((eventName, data) => {
  console.log('game update event: ' + eventName)
  console.log(data)
})

sessions_socket.onAny((eventName, data) => {
  console.log('sessions update event: ' + eventName)
  console.log(data)
})

items_socket.onAny((eventName, data) => {
  console.log('items update event: ' + eventName)
  console.log(data)
})

level_socket.onAny((eventName, data) => {
  console.log('level update event: ' + eventName)
  console.log(data)
})

session_socket.on('disconnect', data => {
	console.error('session socket disconnected')
})

session_socket.on('active_listeners', data => {
	console.log("active listener changed")
  console.log(data)
})

session_socket.on('current_states', data => {
	console.log("current states")
  console.log(data)
})

session_socket.on('quit', data => {
	console.log("session was quit")
  console.log(data)
})

function sendNext(state) {
  session_socket.emit('next', {name:state})
}

function enterState(state) {
  level_socket.emit('enter_state', state)
}

function leaveState() {
  level_socket.emit('leave_state')
}

function leaveLevel() {
  level_socket.disconnect()
}

function userInteraction(data) {
  level_socket.emit('user_interaction', data)
}

/**
 * use userAgent to find out what Browser is in use (not reliable)
 * 
 * see https://code-boxx.com/detect-browser-with-javascript/
 */
 function identifyBrowser() {
    // CHROME
    if (navigator.userAgent.indexOf("Chrome") != -1 ) {
      return("Google Chrome");
    }
    // FIREFOX
    else if (navigator.userAgent.indexOf("Firefox") != -1 ) {
      return("Mozilla Firefox");
    }
    // INTERNET EXPLORER
    else if (navigator.userAgent.indexOf("MSIE") != -1 ) {
      return("Internet Explorer");
    }
    // EDGE
    else if (navigator.userAgent.indexOf("Edge") != -1 ) {
      return("Edge Browser");
    }
    // SAFARI
    else if (navigator.userAgent.indexOf("Safari") != -1 ) {
      return("Safari");
    }
    // OPERA
    else if (navigator.userAgent.indexOf("Opera") != -1 ) {
      return("Opera");
    }
    // YANDEX BROWSER
    else if (navigator.userAgent.indexOf("YaBrowser") != -1 ) {
      return("YaBrowser");
    }
    // OTHERS
    else {
      return("Unknown");
    }
}