import { Session, State, Action } from "./session"
import { Variables } from "./variables"
import { Game } from "./game"
import { Database } from "./mongo"

type SessionInterface = Session & {variables: Variables, action: Action, state: State, date: Date}

type AdaptorFunction = (args: [], {session, game}: {session:SessionInterface, game:Game}) => *

type AdaptorActionResult = undefined | string | Promise<Function>

/**
 * Called if a state was triggered that contains the named action.
 * 
 * The function must have the same name as the action as was defined in actions
 * 
 * If a cancel function is returned, the session will store all action data.
 * 
 * If the adaptor server restarts for any reason, the function is retriggered with all active listeners.
 */
type AdaptorAction = (data: {}, session: SessionInterface) => AdaptorActionResult

export {Database, Session, Game, AdaptorFunction, AdaptorAction, SessionInterface}